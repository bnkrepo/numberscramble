﻿using System;
using Android.App;
using Android.Content;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.OS;
using System.Collections.Generic;
using Android.Graphics;
using Android.Gms.Ads;
using Android.Util;

namespace NumberScramble
{
    [Activity(Label = "Number Scramble", MainLauncher = true, Icon = "@drawable/icon", ScreenOrientation = Android.Content.PM.ScreenOrientation.Portrait)]
    public class MainActivity : Activity
    {
        Button  button1, button2, button3, button4, button5,
                button6, button7, button8, button9, button10, 
                button11, button12, button13, button14, button15, 
                button16, buttonScramble, buttonAbout;
        Random randomNumber = new Random(2);
        List<int> randomNumberList = new List<int>();
        int moveCounter = 0;
        AdView bannerAd;

        protected override void OnCreate(Bundle bundle)
        {
            base.OnCreate(bundle);
            Log.Info("BK", "After OnCreate()");

            // Set our view from the "main" layout resource
            SetContentView(Resource.Layout.Main);

            this.bannerAd = AdWrapper.ConstructStandardBanner(this, AdSize.SmartBanner, "ca-app-pub-7090889053201265/4424612838");// ca-app-pub-2845398947275683/8836803852 - BK
                                                                                                                                  // ca-app-pub-7090889053201265/4424612838 - CodeSproute

            Log.Info("BK", "Created banner ad.");

            this.bannerAd.CustomBuild();

            var layout = FindViewById<LinearLayout>(Resource.Id.linearLayoutAd);
            layout.AddView(this.bannerAd);

            Log.Info("BK", "Banner Ad shown");
            // Get our button from the layout resource,
            // and attach an event to it

            this.button1 = FindViewById<Button>(Resource.Id.button1);
            this.button1.Click += Button1_Click;

            this.button2 = FindViewById<Button>(Resource.Id.button2);
            this.button2.Click += Button2_Click;

            this.button3 = FindViewById<Button>(Resource.Id.button3);
            this.button3.Click += Button3_Click;

            this.button4 = FindViewById<Button>(Resource.Id.button4);
            this.button4.Click += Button4_Click;

            this.button5 = FindViewById<Button>(Resource.Id.button5);
            this.button5.Click += Button5_Click;

            this.button6 = FindViewById<Button>(Resource.Id.button6);
            this.button6.Click += Button6_Click;

            this.button7 = FindViewById<Button>(Resource.Id.button7);
            this.button7.Click += Button7_Click;

            this.button8 = FindViewById<Button>(Resource.Id.button8);
            this.button8.Click += Button8_Click;

            this.button9 = FindViewById<Button>(Resource.Id.button9);
            this.button9.Click += Button9_Click;

            this.button10 = FindViewById<Button>(Resource.Id.button10);
            this.button10.Click += Button10_Click;

            this.button11 = FindViewById<Button>(Resource.Id.button11);
            this.button11.Click += Button11_Click;

            this.button12 = FindViewById<Button>(Resource.Id.button12);
            this.button12.Click += Button12_Click;

            this.button13 = FindViewById<Button>(Resource.Id.button13);
            this.button13.Click += Button13_Click;

            this.button14 = FindViewById<Button>(Resource.Id.button14);
            this.button14.Click += Button14_Click;

            this.button15 = FindViewById<Button>(Resource.Id.button15);
            this.button15.Click += Button15_Click;

            this.button16 = FindViewById<Button>(Resource.Id.button16);
            this.button16.Click += Button16_Click;

            this.buttonScramble = FindViewById<Button>(Resource.Id.buttonScramble);
            this.buttonScramble.Click += ButtonScramble_Click;

            this.buttonAbout = FindViewById<Button>(Resource.Id.buttonAbout);
            this.buttonAbout.Click += delegate
                                        {
                                            StartActivity(typeof(AboutActivity));
                                        };
            moveCounter = 0;

            Log.Info("BK", "Buttons Intiated");

            ScrambleNumbers();

            Log.Info("BK", "Numbers Scrambled.");
        }

        protected override void OnResume()
        {
            if (this.bannerAd != null)
                this.bannerAd.Resume();

            base.OnResume();
        }
        protected override void OnPause()
        {
            if (this.bannerAd != null)
                this.bannerAd.Pause();

            base.OnPause();
        }

        private void ButtonScramble_Click(object sender, EventArgs e)
        {
            ScrambleNumbers();
        }

        private void ScrambleNumbers()
        {
            // get ramdom numbers
            randomNumberList.Clear();
            int number = 0;

            for (int i = 0; i < 16; i++)
            {
                while (randomNumberList.Contains((number = randomNumber.Next(0, 16))) == true && randomNumberList.Count < 16)
                {

                }

                randomNumberList.Add(number);
            }

            // assign them to buttons.
            button1.Text = GetNumber(randomNumberList[0]);
            button2.Text = GetNumber(randomNumberList[1]);
            button3.Text = GetNumber(randomNumberList[2]);
            button4.Text = GetNumber(randomNumberList[3]);
            button5.Text = GetNumber(randomNumberList[4]);
            button6.Text = GetNumber(randomNumberList[5]);
            button7.Text = GetNumber(randomNumberList[6]);
            button8.Text = GetNumber(randomNumberList[7]);
            button9.Text = GetNumber(randomNumberList[8]);
            button10.Text = GetNumber(randomNumberList[9]);
            button11.Text = GetNumber(randomNumberList[10]);
            button12.Text = GetNumber(randomNumberList[11]);
            button13.Text = GetNumber(randomNumberList[12]);
            button14.Text = GetNumber(randomNumberList[13]);
            button15.Text = GetNumber(randomNumberList[14]);
            button16.Text = GetNumber(randomNumberList[15]);

            moveCounter = -1;
            IncrementMove();

            LinearLayout mainLayout = FindViewById<LinearLayout>(Resource.Id.linearLayout1);
            mainLayout.SetBackgroundColor(Color.DarkKhaki);
        }

        private string GetNumber(int number)
        {
            if (number == 0)
                return "";
            else
                return number.ToString();
        }

        private bool CheckAndExchangeText(Button btn1, Button btn2)
        {
            if (string.IsNullOrEmpty(btn2.Text) == true)
            {
                btn2.Text = btn1.Text;
                btn1.Text = "";
                IncrementMove();
                return true;
            }

            return false;
        }

        private void IncrementMove()
        {
            moveCounter++;

            TextView txtMoves = FindViewById<TextView>(Resource.Id.textViewMoves);

            bool bWon = CheckForWin();

            if (bWon == true)
                txtMoves.Text = "Moves: " + moveCounter.ToString() + " - You Won!";
            else
                txtMoves.Text = "Moves: " + moveCounter.ToString();
            
        }

        private bool CheckForWin()
        {
            if (this.button1.Text == "1" && this.button2.Text == "2" && this.button3.Text == "3" && this.button4.Text == "4" &&
                this.button5.Text == "5" && this.button6.Text == "6" && this.button7.Text == "7" && this.button8.Text == "8" &&
                this.button9.Text == "9" && this.button10.Text == "10" && this.button11.Text == "11" && this.button12.Text == "12" &&
                this.button13.Text == "13" && this.button14.Text == "14" && this.button15.Text == "15" && this.button16.Text == "")
            {
                LinearLayout mainLayout = FindViewById<LinearLayout>(Resource.Id.linearLayout1);
                mainLayout.SetBackgroundColor(Color.BlueViolet);
                return true;
            }
            return false;
        }

        private void Button16_Click(object sender, EventArgs e)
        {
            if (CheckAndExchangeText(button16, button12) == false)
                CheckAndExchangeText(button16, button15);            
        }

        private void Button15_Click(object sender, EventArgs e)
        {
            if (CheckAndExchangeText(button15, button11) == false)
                if (CheckAndExchangeText(button15, button14) == false)
                    CheckAndExchangeText(button15, button16);            
        }

        private void Button14_Click(object sender, EventArgs e)
        {
            if (CheckAndExchangeText(button14, button10) == false)
                if (CheckAndExchangeText(button14, button13) == false)
                    CheckAndExchangeText(button14, button15);           
        }

        private void Button13_Click(object sender, EventArgs e)
        {
            if (CheckAndExchangeText(button13, button9) == false)
                    CheckAndExchangeText(button13, button14);
        }

        private void Button12_Click(object sender, EventArgs e)
        {
            if (CheckAndExchangeText(button12, button8) == false)
                if (CheckAndExchangeText(button12, button11) == false)
                    CheckAndExchangeText(button12, button16);
        }

        private void Button11_Click(object sender, EventArgs e)
        {
            if (CheckAndExchangeText(button11, button7) == false)
                if (CheckAndExchangeText(button11, button10) == false)
                    if (CheckAndExchangeText(button11, button12) == false)
                        CheckAndExchangeText(button11, button15);
        }

        private void Button10_Click(object sender, EventArgs e)
        {
            if (CheckAndExchangeText(button10, button6) == false)
                if (CheckAndExchangeText(button10, button9) == false)
                    if (CheckAndExchangeText(button10, button11) == false)
                        CheckAndExchangeText(button10, button14);
        }

        private void Button9_Click(object sender, EventArgs e)
        {
            if (CheckAndExchangeText(button9, button5) == false)
                if (CheckAndExchangeText(button9, button10) == false)
                    CheckAndExchangeText(button9, button13);
        }

        private void Button8_Click(object sender, EventArgs e)
        {
            if (CheckAndExchangeText(button8, button4) == false)
                if (CheckAndExchangeText(button8, button7) == false)
                    CheckAndExchangeText(button8, button12);
        }

        private void Button7_Click(object sender, EventArgs e)
        {
            if (CheckAndExchangeText(button7, button3) == false)
                if (CheckAndExchangeText(button7, button6) == false)
                    if (CheckAndExchangeText(button7, button8) == false)
                        CheckAndExchangeText(button7, button11);
        }

        private void Button6_Click(object sender, EventArgs e)
        {
            if (CheckAndExchangeText(button6, button2) == false)
                if (CheckAndExchangeText(button6, button5) == false)
                    if (CheckAndExchangeText(button6, button7) == false)
                        CheckAndExchangeText(button6, button10);
        }

        private void Button5_Click(object sender, EventArgs e)
        {
            if (CheckAndExchangeText(button5, button1) == false)
                if (CheckAndExchangeText(button5, button6) == false)
                    CheckAndExchangeText(button5, button9);
        }

        private void Button4_Click(object sender, EventArgs e)
        {
            if (CheckAndExchangeText(button4, button3) == false)
                    CheckAndExchangeText(button4, button8);
        }

        private void Button3_Click(object sender, EventArgs e)
        {
            if (CheckAndExchangeText(button3, button2) == false)
                if (CheckAndExchangeText(button3, button4) == false)
                    CheckAndExchangeText(button3, button7);
        }

        private void Button2_Click(object sender, EventArgs e)
        {
            if (CheckAndExchangeText(button2, button1) == false)
                if (CheckAndExchangeText(button2, button3) == false)
                    CheckAndExchangeText(button2, button6);
        }

        private void Button1_Click(object sender, EventArgs e)
        {
            if (CheckAndExchangeText(button1, button2) == false)
                CheckAndExchangeText(button1, button5);                
        }
    }
}

