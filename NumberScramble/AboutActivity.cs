using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.Graphics;

namespace NumberScramble
{
    [Activity(Label = "About Number Scramble", ScreenOrientation = Android.Content.PM.ScreenOrientation.Portrait)]
    public class AboutActivity : Activity
    {
        TextView txtVersion;

        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            // Set about view for this activity
            SetContentView(Resource.Layout.About);

            LinearLayout aboutLayout = FindViewById<LinearLayout>(Resource.Id.linearLayoutAbout);
            aboutLayout.SetBackgroundColor(Color.DarkKhaki);

            Context context = this.ApplicationContext;
            var name = context.PackageManager.GetPackageInfo(context.PackageName, 0).VersionName;
            //var code = context.PackageManager.GetPackageInfo(context.PackageName, 0).VersionCode;

            this.txtVersion = FindViewById<TextView>(Resource.Id.textViewVersion);
            this.txtVersion.Text = "Version: " + name;

        }
    }
}